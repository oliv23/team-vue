import teamIndexComponent from './components/teamIndex.vue'
import addTeamMemberComponent from './components/addTeamMember.vue'
import notFoundComponent from './components/notFound.vue'

var routes = [
    { 
    	path: '/', 
    	redirect: '/team' 
    },
	{ 
		path: '/team', 
		component: teamIndexComponent 
	},
	{ 
		path: '/add', 
		component: addTeamMemberComponent 
	},
	{ 
		path: '*', 
		component: notFoundComponent
	}
]

export default routes;