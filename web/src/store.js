export var store = {
  debug: true,
  state: {
    members: [],
    filters: {
      department: "all",
      name: "",
      title: "",
      location: "",
      page: 1
    },
    membersPerPage: 5,
    apiError: ""
  },
  setMembersAction(newValue) {
    if (this.debug) console.log('setMembersAction triggered with', newValue);
    this.state.members = newValue;
  },
  clearMembersAction () {
    if (this.debug) console.log('clearMembersAction triggered');
    this.state.members = [];
  },
  pushMemberAction(newValue) {
    if (this.debug) console.log('pushMemberAction triggered with', newValue);
    this.state.members.push(newValue);
  },
  removeMemberAction(index) {
    if (this.debug) console.log('removeMemberAction triggered with', index);
    this.state.members = this.state.members.filter(x => { return x.id != index;});
  },
  setFilterAction(filter, newValue) {
    if (this.debug) console.log('setFilterAction triggered with', newValue);
    this.state.filters[filter] = newValue;
  },
  clearFilterAction(filter) {
    if (this.debug) console.log('clearFilterAction triggered');
    this.state.filters[filter] = '';
  },
  setApiErrorAction(newValue) {
    if (this.debug) console.log('setApiErrorAction triggered with', newValue);
    this.state.apiError = newValue;
  }
}