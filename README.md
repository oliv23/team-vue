
# team-vue

## Project architecture
 - Front end (Vue.js) located in the `web` folder
 - Back end (Hapi.js) located in the `api` folder
 - Database: SQLite

## How I built this
 1. Built out the entire front end with Vue.js, using a local
    store to save and update data.
 2. Once finished, I moved the front end to its own folder.
 3. Created a new folder for the back-end and built out API routes as well as main index route to serve the front-end's `index.html` entry file.
 4. Updated front end to use the API instead of a local store.

## Project setup

### 1. Install
Custom script will install packages for both back and front end with a single command:
```
npm run install
```

### 2. Build
Script will build Vue.js app to be served by Hapi.js server.
```
npm run build
```

### 3. Serve
Start the Hapi.js server
```
npm run start
```

## Features

 - List members
 - Add members
 - Remove a member
 - Filter members by name, title, location, department
 - Pagination (5/page)
 - Use of API to fetch/save/remove team members
 
 ## Future features and nice-to-haves
 
 - Proper security for the API routes
 - Server side image processing of the team member's uploaded picture (automatically crop it to a square and an appropriate size)
 - Adding the **edit user** functionality
 - Testing
 - Ability to define new departments: create a new "TeamMemberDepartment" model for SQL normalization and add a form to create departments 
 - Use a more production-ready database (PostgreSQL for example)
 - Better Front-end data validation
 - Adding Server-side validation