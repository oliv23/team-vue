'use strict';

var db = require("./db/init");
var Hapi = require('hapi');
var models = require("./db/models");
var populate = require('./db/populate');

var server = new Hapi.Server({
    host: '0.0.0.0',
    port: +process.env.PORT || 3000
});

async function start() {
    try {
        // Register inert package to serve static files
        await server.register([  
            require('@hapi/inert')
        ]);

        // Require routes
        server.route(require('./lib/routes'));

        // Sync database models
        await db.sync({force: false});

        // Populate database if empty
        await populate();

        // Start
        await server.start();

        console.log('Server running at: ', server.info.uri);
    }
    catch(err) {
        console.log(err);
    };
}

start();