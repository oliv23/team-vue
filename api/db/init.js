var env = process.env.NODE_ENV || "development";
var config = require('../config/db')[env];
var Sequelize = require("sequelize");
var sequelize = new Sequelize(config.database, config.username, config.password, config);

module.exports = sequelize;