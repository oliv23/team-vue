var models = require('./models');

// Populate database with dummy users
var populate = async function() {
    var members_count = await models.TeamMember.count();

    if (members_count == 0) {
		var members = [
	      {
	        name: 'Olivier Breton',
	        picture: 'https://vuejs.org/images/logo.png',
	        title: 'Software engineer',
	        location: 'New York',
	        department: 'Engineering'
	      },
	      {
	        name: 'John Doe',
	        picture: 'https://vuejs.org/images/logo.png',
	        title: 'Lead engineer',
	        location: 'San Francisco',
	        department: 'Design'
	      },
	      {
	        name: 'Jane Doe',
	        picture: 'https://vuejs.org/images/logo.png',
	        title: 'QA Assistant',
	        location: 'New York',
	        department: 'QA'
	      }
	    ];

	    await models.TeamMember.bulkCreate(members);
    }
}

module.exports = populate;