var Sequelize = require('sequelize');
var sequelize = require("./init");

var TeamMember = sequelize.define("teammember", {
	name: {type: Sequelize.STRING, allowNull: false},
	picture: {type: Sequelize.TEXT, allowNull: false},
	title: {type: Sequelize.STRING, allowNull: false},
	location: {type: Sequelize.STRING, allowNull: false},
	department: {type: Sequelize.STRING, allowNull: false}
});

module.exports = {
	TeamMember: TeamMember
}