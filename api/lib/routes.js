var Joi = require('joi');
var models = require("../db/models");
var sequelize = require("../db/init");
var Sequelize = require('sequelize');

module.exports = [
	{  
		method: 'GET',
		path: '/{any*}',
		handler: {
			directory: {
				path: '../web/dist'
			}
		}
	},
	{
		method: 'GET',
		path: '/api/members',
		config: {
			handler: async function(request, h) {
				try {
					var team_members = await models.TeamMember.findAll({});

					return {
						status: "success",
						data: {
							members: team_members
						}
					}
				}
				catch(e) {
					console.log(e);
					return {
						status: "error",
						message: e
					};
				}
			}
		}
	},
	{
		method: 'POST',
		path: '/api/members',
		config: {
			handler: async function(request, h) {
				try {
					var new_member = {
						name: request.payload.name,
						picture: request.payload.picture,
						title: request.payload.title,
						location: request.payload.location,
						department: request.payload.department
					};

					var db_member = await models.TeamMember.create(new_member);

					return {
						status: "success",
						data: {
							member: db_member
						}
					};
				}
				catch(e) {
					console.log(e);
					return {
						status: "error",
						message: e
					};
				}
			}
		}
	},
	{
		method: 'DELETE',
		path: '/api/members/{id}',
		config: {
			handler: async function(request, h) {
				try {
					// Remove user
					await models.TeamMember.destroy({
						where: {
							id: request.params.id
						}
					});

					return {
						status: "success",
						data: null
					};
				}
				catch(e) {
					console.log(e);
					return {
						status: "error",
						message: e
					};
				}
			}
		}
	}
]